<div class="container mt-3">

    <div class="card">
        <div class="card-header fw-bold">
            <?= $data['mhs']['nama']; ?>
        </div>
        <div class="card-body">
            <p class="card-text">
                <?= $data['mhs']['nis']; ?>
            </p>
            <p class="card-text">
                <?= $data['mhs']['kelas']; ?>
            </p>
            <p class="card-text">
                <?= $data['mhs']['jurusan']; ?>
            </p>
            <a href="<?= BASEURL; ?>/index.php/mahasiswa" class="btn btn-primary">Back</a>
        </div>
    </div>

</div>