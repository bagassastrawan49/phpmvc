<div class="container mt-3">
    <div class="row">
        <div class="col-6">

            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#FormModal">
                + Insert Data Mahasiswa
            </button>

            <br><br>
            <h3>Daftar Mahasiswa</h3>

            <ul class="list-group">
                <?php foreach ($data['mhs'] as $mhs) : ?>
                <li class="list-group-item d-flex justify-content-between justify-content-center" aria-current="true">
                    <?= $mhs['nama'] ?>
                    <a href="<?= BASEURL; ?>/index.php/mahasiswa/detail/<?= $mhs['id'] ?>"
                        class=" badge bg-dark">detalis</a>
                </li>
                <?php endforeach; ?>
            </ul>


        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="FormModal" tabindex="-1" aria-labelledby="FormModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">Insert Data Mahasiswa
                <h5 class="modal-title" id="FormModal"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form action="<?= BASEURL; ?>/mahasiswa/tambah" method="post">
                    <div class=" form-group">
                        <label for="nama" class="form-label">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" placeholder="name">
                    </div>

                    <div class=" form-group">
                        <label for="nis" class="form-label">NIS</label>
                        <input type="number" class="form-control" id="nis" name="nis" placeholder="input nis">
                    </div>

                    <div class=" form-group">
                        <label for="kelas" class="form-label">Kelas</label>
                        <input type="text" class="form-control" id="kelas" name="kelas" placeholder="input Kelas">
                    </div>

                    <div class="form-group">
                        <label for="jurusan">Jurusan</label>
                        <select class="form-control" id="jurusan" name="jurusan">
                            <option value="Rekayasa Perangkat Lunak">Rekayasa Perangkat Lunak</option>
                            <option value="Teknik Komputer Jaringan">Teknik Komputer Jaringan</option>
                            <option value="Multimedia">Multimedia</option>
                        </select>
                    </div>
            </div>
            <div class=" modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Insert Data</button>
            </div>
            </form>
        </div>
    </div>
</div>